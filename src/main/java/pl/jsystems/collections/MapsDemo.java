package pl.jsystems.collections;

import java.util.HashMap;
import java.util.Map;

class MapsDemo {

    public static void main(String[] args) {

        Map<Integer, String> integerStringMap = new HashMap<>();
        integerStringMap.put(1, "one");
        integerStringMap.put(3, "three");
        integerStringMap.put(2, "two");
        integerStringMap.put(4, "four");
        integerStringMap.put(2, "xxxxx");

        for (var entry : integerStringMap.entrySet()) {
            System.out.println(String.format("%s <-> %s", entry.getKey(), entry.getValue()));
        }

        String existingValue = integerStringMap.get(4);
        System.out.println(String.format("Existing value: %s", existingValue));

        int keyToCheck = 20;
        if (integerStringMap.containsKey(keyToCheck)) {
            String theValue = integerStringMap.get(keyToCheck);
            System.out.println(String.format("The value: %s", theValue));
        } else {
            System.out.println("The key doesn't exist");
        }
    }
}
