package pl.jsystems.collections;

import java.util.LinkedList;
import java.util.List;

class ListsDemo {

    public static void main(String[] args) {
        //Lists
        //List<Integer> numbers = new ArrayList<>();
        List<Integer> numbers = new LinkedList<>();
        numbers.add(5);
        numbers.add(1);
        numbers.add(5);
        numbers.add(8);
        System.out.println(String.format("Initial size of the list: %s", numbers.size()));

        //numbers.clear();

        //numbers.forEach(System.out::println);
        //numbers.forEach(n-> System.out.println(n));
        /*
        numbers.forEach(n -> {
            System.out.println(n);
        });
        */
        showList(numbers);

        Integer specifiedValue = numbers.get(2);
        System.out.println(String.format("List(2)=%s", specifiedValue));

        System.out.println("After removal");
        numbers.remove(Integer.valueOf(5));
        showList(numbers);

        boolean containsFiveValue = numbers.contains(5);
        System.out.println(String.format("If our list contains 5? : %s", containsFiveValue));

        boolean containsFourValue = numbers.contains(4);
        System.out.println(String.format("If our list contains 4? : %s", containsFourValue));

        numbers.add(1, 70);

        List<Integer> anotherList = List.of(6, 8, 9, 0, 2, 7);

        //numbers.addAll(strings) <- konflikt typów
        numbers.addAll(anotherList);

        int theLastElementIndex = numbers.size() - 1;
        numbers.set(theLastElementIndex, 9);
        showList(numbers);
        System.out.println(String.format("Eventual size of the list: %s", numbers.size()));
    }

    private static void showList(List<Integer> numbers) {
        for (Integer number : numbers) {
            System.out.println(number);
        }
    }
}
