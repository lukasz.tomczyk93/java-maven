package pl.jsystems.collections;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

class SetsDemo {

    public static void main(String[] args) {
        Set<String> strings = new HashSet<>();
        strings.add("c");
        strings.add("a");
        strings.add("a");
        strings.add("a");
        strings.add("d");
        strings.add("b");
        strings.add("a");

        showSetContent(strings);
        boolean isStringsEmpty = strings.isEmpty();
        System.out.println(String.format("Is set empty? : %s", isStringsEmpty));

        System.out.println("Linked Hash Set:");
        Set<String> linkedStrings = new LinkedHashSet<>();
        linkedStrings.add("c");
        linkedStrings.add("a");
        linkedStrings.add("a");
        linkedStrings.add("a");
        linkedStrings.add("d");
        linkedStrings.add("b");
        linkedStrings.add("a");
        showSetContent(linkedStrings);

        System.out.println("Tree Set");
        Set<Integer> integers = new TreeSet<>();
        integers.add(23);
        integers.add(1);
        integers.add(3);
        integers.add(2);
        integers.add(1);
        integers.add(6);
        integers.add(5);
        integers.add(6);
        integers.add(9);
        integers.add(0);
        showSetContentInts(integers);
    }

    private static void showSetContent(Set<String> strings) {
        for (String string : strings) {
            System.out.println(string);
        }
    }

    private static void showSetContentInts(Set<Integer> integers) {
        for (Integer integer : integers) {
            System.out.println(integer);
        }
    }
}
