package pl.jsystems.abstractclass;

abstract class Animal {

    abstract void getSound();
}
