package pl.jsystems.abstractclass;

import java.util.List;

class Main {

    public static void main(String[] args) {
        Animal cat = new Cat();
        Animal dog = new Dog();

        List<Animal> animals = List.of(cat, dog);
        for (Animal animal : animals) {
            animal.getSound();
        }
    }
}
