package pl.jsystems.abstractclass;

class Cat extends Animal {

    @Override
    void getSound() {
        System.out.println("Miau");
    }
}
