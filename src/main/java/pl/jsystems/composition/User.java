package pl.jsystems.composition;

class User extends Person {

    private final Address address;

    public User(Address address, int id) {
        this.address = address;
        this.id = id;
    }

    public User(int id, Address address) {
        this.address = address;
        this.id = id;
    }

    Address getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", address=" + address +
                '}';
    }
}
