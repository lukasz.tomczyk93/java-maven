package pl.jsystems.composition;

class Address {

    private final String street;
    private final String postalCode;


    Address(String street, String postalCode) {
        this.street = street;
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
