package pl.jsystems.composition;

import java.util.Random;

class Main {

    public static void main(String[] args) {
        int userId = new Random().nextInt(1, 200);
        var userAddress = new Address("Aleja Solidarności", "20780");
        User user = new User(userId, userAddress);
        user.showId();
        System.out.println(user.toString());
    }
}
