package pl.jsystems.exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

class ExceptionsDemo {

    public static void main(String[] args) {
        var scanner = new Scanner(System.in);
        int theInput = readInt(scanner);
        System.out.println(theInput);

        try {
            String nullString = null;
            int length = nullString.length();
        } catch (NullPointerException exception) {
            System.out.println("String was null");
        }
    }

    private static int readInt(Scanner scanner) throws CouldNotReadNumberFromConsoleException {
        try {
            return scanner.nextInt();
        } catch (InputMismatchException exception) {
            System.out.println("The input must be int!");
        } catch (IllegalStateException exception) {
            System.out.println("The scanner was closed");
        }
        throw new CouldNotReadNumberFromConsoleException();
    }
}
