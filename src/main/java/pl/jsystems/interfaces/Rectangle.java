package pl.jsystems.interfaces;

class Rectangle implements Shape {

    private final Double a;
    private final Double b;

    Rectangle(Double a, Double b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public double calculatePerimeter() {
        return (2 * a + 2 * b);
    }

    @Override
    public void printShapeName() {
        System.out.println("Rectangle");
    }
}
