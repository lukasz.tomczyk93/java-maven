package pl.jsystems.interfaces;

interface Shape {

    double calculatePerimeter();

    default void printShapeName() {
        throw new IllegalStateException("Not implemented yet");
    }
}
