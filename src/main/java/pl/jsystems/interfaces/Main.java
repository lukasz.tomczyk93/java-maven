package pl.jsystems.interfaces;

import java.util.Set;

class Main {

    public static void main(String[] args) {
        Shape circle = new Circle(1.3);
        Shape rectangle = new Rectangle(23D, 12D);

        Set<Shape> shapes = Set.of(circle, rectangle);
        for (Shape shape : shapes) {
            shape.printShapeName();
            System.out.println(shape.calculatePerimeter());
        }
    }
}
