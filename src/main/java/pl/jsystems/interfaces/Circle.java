package pl.jsystems.interfaces;

class Circle implements Shape {

    private final Double radius;

    Circle(Double radius) {
        this.radius = radius;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public void printShapeName() {
        System.out.println("Circle");
    }
}
