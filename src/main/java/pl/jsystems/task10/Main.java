package pl.jsystems.task10;

class Main {

    public static void main(String[] args) {
        Author author1 = new Author("Author 1", "Nationality 1");
        Author author2 = new Author("Author 2", "Nationality 2");

        Poem poem1 = new Poem(author1, 8);
        Poem poem2 = new Poem(author1, 4);
        Poem poem3 = new Poem(author2, 13);
        Poem poem4 = new Poem(author2, 7);

        Poem[] poems = {poem1, poem2, poem3, poem4};

        Poem theLongestPoem = poems[0];
        for (Poem poem : poems) {
            if (poem.getStropheNumbers() > theLongestPoem.getStropheNumbers()) {
                theLongestPoem = poem;
            }
        }

        System.out.println(String.format("The longest poem: %s", theLongestPoem));
    }
}
