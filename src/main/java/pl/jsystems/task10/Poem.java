package pl.jsystems.task10;

class Poem {

    private Author creator;
    private Integer stropheNumbers;

    Poem(Author creator, Integer stropheNumbers) {
        this.creator = creator;
        this.stropheNumbers = stropheNumbers;
    }

    Integer getStropheNumbers() {
        return stropheNumbers;
    }

    Author getCreator() {
        return creator;
    }

    @Override
    public String toString() {
        return "Poem{" +
                "creator=" + creator +
                ", stropheNumbers=" + stropheNumbers +
                '}';
    }
}
