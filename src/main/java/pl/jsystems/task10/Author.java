package pl.jsystems.task10;

class Author {

    private final String fullName;
    private final String nationality;

    public Author(String fullName, String nationality) {
        this.fullName = fullName;
        this.nationality = nationality;
    }

    @Override
    public String toString() {
        return "Author{" +
                "fullName='" + fullName + '\'' +
                ", nationality='" + nationality + '\'' +
                '}';
    }
}
